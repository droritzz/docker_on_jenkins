## This project contains
- a simple flask app 
- dockerfile for an alpine image 
- jenkinsfile to build the docker image and run the container

## Requirements
jenkins server (in this project Centos7 running from Vagrant) with installed plugins and following packages:
- Git
- Docker
- Python 
- Flask
